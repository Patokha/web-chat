const express = require('express');
const feedRoutes = require('./routes/feed');
const authRoutes = require('./routes/auth');

const bodyParser = require('body-parser');
const app = express();
const mongoose = require('mongoose');

const socket = require('./socket.js');
const {config} = require('./seed/config');
const {APP_USER, APP_PSWD, APP_PORT} = config;
app.use(bodyParser.json());

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  next();
});

app.use('/feed', feedRoutes);
app.use('/auth', authRoutes);

app.use((error, req, res, next) => {
  const {statusCode: status = 500, message, data = []} = error;
  res.status(status).json({message, data});
});

(async () => {
  try {
    await mongoose.connect(`mongodb://${APP_USER}:${APP_PSWD}@spa-shard-00-00-r1428.mongodb.net:27017,spa-shard-00-01-r1428.mongodb.net:27017,spa-shard-00-02-r1428.mongodb.net:27017/test?ssl=true&replicaSet=SPA-shard-0&authSource=admin&retryWrites=true&w=majority`, {useNewUrlParser: true, useUnifiedTopology: true});
    const server = await app.listen(APP_PORT);
    const io = socket.init(server);
    io.on('connection', (socket) => {
      console.log('Client connected');
    });
    console.log(`Connected to port: ${APP_PORT}`);
  } catch (error) {
    console.log('ERROR', error);
  }
})();

