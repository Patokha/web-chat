const dotenv = require('dotenv');
dotenv.config();

const {
  JWT_TOKEN_SECRET,
  APP_PSWD,
  APP_USER,
  APP_PORT,
} = process.env;

exports.config = {
  JWT_TOKEN_SECRET,
  APP_PSWD,
  APP_USER,
  APP_PORT,
};
