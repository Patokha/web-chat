fetch('http://localhost:8080/feed/posts', {
  headers: {
    Authorization: `Bearer ${token}`,
  },
})
  .then((r) => r.json())
  .then((data) => console.log(data))
  .catch((err) => console.log(err));
