fetch(`http://localhost:8080/feed/post/${id}`, {
  method: 'PUT',
  body: JSON.stringify({
    title: 'Some title for testing',
    content: 'Some dummy description',
    creator: {
      name: 'Ivan',
    },
    imageUrl: 'http://somedummyimage.png',
  }),
  headers: {
    "Authorization": `Bearer ${token}`,
    'Content-Type': 'application/json',
  },
});
