fetch(`http://localhost:8080/feed/post/${id}`, {
  method: 'DELETE',
  headers: {
    "Authorization": `Bearer ${token}`,
    'Content-Type': 'application/json',
  },
});
