const sinon = require('sinon');
const expect = require('chai');

const User = require('../models/users.js');

const authController = require('../controllers/auth');

describe('Auth controller - Login', function() {
  it('should throw 500 an error is accessing the DB fails', (done) => {
    sinon.stub(User, 'findOne');
    User.findOne.throws();
    const req = {
      body: {
        email: 'test@gmail.com',
        password: 'dfshfsdjf',
      },
    };
    authController.loginUser(req, {}, ()=> {})
      .then((result) => {
        expect(result).to.be.an('error');
        expect(result).to.have.property('statusCode', 500);
        done();
      }).catch(done());
    User.findOne.restore();
  });
});
