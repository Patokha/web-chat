/* eslint-env , mocha */
const {expect} = require('chai');
const isAuth = require('../middlewere/auth');
const jwt = require('jsonwebtoken');
const sinon = require('sinon');
// unit test

describe('Auth middleware', () => {
  it('should throw an error of no authorization header is present', () => {
    const req = {
      get: function() {
        return null;
      },
    };
    expect(isAuth.bind(this, req, {}, ()=>{})).to.throw('Access denied. No token provided');
  });

  it('should throw an error of invalid token provided', () => {
    const req = {
      get: function() {
        return 'xyz';
      },
    };

    expect(isAuth.bind(this, req, {}, ()=>{})).to.throw();
  });

  it('should yield a userId after decoding the token', () => {
    const req = {
      get: function(headerToken) {
        return 'Bearer dsfjksdflksdjflk;sadfj';
      },
    };

    sinon.stub(jwt, 'decode');
    jwt.decode.returns({userId: 'slkdjskl213'});
    isAuth(req, {}, ()=>{});
    expect(req).to.have.property('userId');
    expect(req).to.have.property('userId', 'slkdjskl213');
    expect(jwt.decode.called).to.be;
    jwt.decode.restore();
  });

  it('should throw an error if the token connot be verified', () => {
    const req = {
      get: function(headerToken) {
        return 'Bearer xyz';
      },
    };
    expect(isAuth.bind(this, req, {}, () => {})).to.throw();
  });
});
