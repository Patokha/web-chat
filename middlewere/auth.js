const jwt = require('jsonwebtoken');
const config = require('../seed/config.js');
const {JWT_TOKEN_SECRET} = config;
module.exports = (req, res, next) => {
  let decodeToken;
  try {
    const authorizationToken = req.get('Authorization');
    if (!authorizationToken) {
      const error = new Error('Access denied. No token provided');
      error.statusCode = 401;
      throw error;
    }

    const token = authorizationToken.split(' ')[1];
    decodeToken = jwt.decode(token, JWT_TOKEN_SECRET);
  } catch (error) {
    console.log(error);
    error.statusCode = 500;
    throw error;
  }

  if (!decodeToken) {
    const error = new Error('Access denied. Invalid token');
    error.statusCode = 401;
    throw error;
  }

  req.userId = decodeToken.userId;
  next();
};
