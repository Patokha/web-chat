const check = require('express-validator');
const {validationResult} = check;
const Post = require('../models/posts');
const User = require('../models/users.js');

const socket = require('../socket');
exports.getPosts = async (req, res, next) => {
  try {
    const {page: currentPage = 1} = req.query;
    const itemsPerPage = 4;
    const totalItems = await Post.find().countDocuments();
    const posts = await Post
      .find()
      .populate('creator')
      .sort({createdAt: -1})
      .skip((currentPage - 1) * itemsPerPage)
      .limit(itemsPerPage);
    return res.status(200).json({
      status: 'Fetched posts successfully',
      posts,
      totalItems,
    });
  } catch (err) {
    if (!err.statusCode) err.statusCode = 500;
    next(err);
  }
};

exports.createPost = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    console.log(errors.array());
    const error = new Error('Validation incorrect.');
    error.statusCode = 422;
    error.data = errors.array().map(({value, msg, param}) => ({value, msg, param}));
    throw error;
  }

  try {
    const {title, content, imageUrl} = req.body;
    const post = new Post({
      title,
      content,
      creator: req.userId,
      imageUrl,
    });
    await post.save();
    const user = await User.findById(req.userId);
    user.posts = [...user.posts, post._id];
    await user.save();
    // emit - for all users
    // broadcast - for all except for creator of the post
    socket.getIO().emit('posts', {
      action: 'create',
      post,
      userName: user.name,
    });
    res.status(201).json({
      message: 'Post created successfully',
      post,
      creator: {
        _id: user._id,
        name: user.name,
      },
    });
  } catch (err) {
    if (!err.statusCode) err.statusCode = 500;
    next(err);
  }
};

exports.getPostDetails = async (req, res, next) => {
  try {
    const {id} = req.params;
    const post = await Post.findById(id);
    if (!post) {
      const error = new Error('COuld not find post with such ID');
      error.statusCode = 404;
      throw error;
    }

    res.status(200).json({
      status: 'Post fetched successfully',
      post,
    });
  } catch (err) {
    if (!err.statusCode) err.statusCode = 500;
    next(err);
  }
};

exports.changePost = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    console.log(errors.array());
    const error = new Error('Validation incorrect.');
    error.statusCode = 422;
    error.data = errors.array().map(({value, msg, param}) => ({value, msg, param}));
    throw error;
  }
  try {
    const {id} = req.params;
    const {imageUrl, title, content} = req.body;
    const post = await Post.findById(id).populate('creator');
    if (!post) {
      const error = new Error('No such post in DB');
      error.statusCode = 404;
      throw error;
    }
    if (post.creator._id.toString() === req.userId) {
      const user = await User.findById(req.userId);
      post.imageUrl = imageUrl;
      post.title = title;
      post.creator = req.userId,
      post.content = content;
      const result = await post.save();
      socket.getIO().emit('posts', {
        action: 'update',
        post,
        userName: user.name,
      });
      res.status(200).json({
        message: 'Post updated successfully',
        post: {
          ...result._doc,
        },
      });
    } else {
      const error = new Error('Not authorized');
      error.statusCode = 403;
      throw error;
    }
  } catch (err) {
    if (!err.statusCode) err.statusCode = 500;
    next(err);
  }
};

exports.deletePost = async (req, res, next) => {
  try {
    const {id} = req.params;
    const post = await Post.findById(id);
    if (!post) {
      const error = new Error('No such post in DB');
      error.statusCode = 404;
      throw error;
    }

    if (post.creator.toString() !== req.userId) {
      const error = new Error('Not authorized');
      error.statusCode = 403;
      throw error;
    }

    await Post.findByIdAndRemove(post._id, {useFindAndModify: false});
    const user = await User.findById(req.userId);
    user.posts = user.posts.filter((i) => i.toString() !== id);
    await user.save();
    socket.getIO().emit('posts', {
      action: 'delete',
      post,
      userName: user.name,
    });
    res.status(200).json({
      message: 'Post deleted successfully',
      postId: id,
    });
  } catch (err) {
    if (!err.statusCode) err.statusCode = 500;
    next(err);
  }
};
