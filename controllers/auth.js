const check = require('express-validator');
const {validationResult} = check;
const User = require('../models/users');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {config} = require('../seed/config');
const {JWT_TOKEN_SECRET} = config;

exports.signupUser = async (req, res, next) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      const error = new Error('User validation incorrect.');
      error.statusCode = 422;
      error.data = errors.array().map(({value, msg, param}) => ({value, msg, param}));
      throw error;
    }

    const {name, email, password} = req.body;
    const cryptedPassword = await bcrypt.hash(password, 12);
    const user = new User({
      name,
      email,
      password: cryptedPassword,
    });
    const {_id: userId} = await user.save();
    res.status(201).json({
      message: 'New user was successfuly created',
      userId,
    });
  } catch (err) {
    if (!err.statusCode) err.statusCode = 500;
    next(err);
  }
};

exports.loginUser = async (req, res, next) => {
  try {
    const {email, password} = req.body;
    const user = await User.findOne({email});
    if (!user) {
      const error = new Error('A user with his email could not be found.');
      error.statusCode = 401;
      throw error;
    }

    const isPasswordsMatch = await bcrypt.compare(password, user.password);
    if (!isPasswordsMatch) {
      const error = new Error('Invalid email or password');
      error.statusCode = 401;
      throw error;
    }

    const token = jwt.sign({
      email: user.email,
      userId: user._id.toString(),
    },
    JWT_TOKEN_SECRET,
    {expiresIn: '1h'});

    res.status(200).json({token, userId: user._id.toString()});
    return;
  } catch (err) {
    if (!err.statusCode) err.statusCode = 500;
    next(err);
    return err;
  }
};
