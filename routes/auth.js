const express = require('express');
const router = express.Router();
const {check} = require('express-validator');
const {signupUser, loginUser} = require('../controllers/auth');

const User = require('../models/users');

const validateParams = () => {
  return [
    check('email')
      .isEmail()
      .withMessage('Please enter a valid email')
      .normalizeEmail()
      .custom(async (value) => {
        const user = await User.findOne({email: value});
        if (user) return Promise.reject('Email is already in use.');
      }),

    check('password')
      .trim()
      .not()
      .isEmpty()
      .isLength({min: 8})
      .withMessage('Please enter a password at least 8 symbols'),
    check('name')
      .trim()
      .isLength({min: 3})
      .withMessage('Please enter a correct name. More than 5 symbols'),
  ];
};

router.post('/signup', validateParams(), signupUser);
router.post('/login', loginUser);
module.exports = router;
