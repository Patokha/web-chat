const express = require('express');
const router = express.Router();
const check = require('express-validator');
const {body} = check;
const {getPosts, createPost, getPostDetails, changePost, deletePost} = require('../controllers/feed');
const isAuth = require('../middlewere/auth.js');
const validateParams = () => {
  return [
    body('imageUrl').trim().isURL().withMessage('Please enter a url of an image'),
    body('title').trim().isLength({min: 5}).withMessage('Please enter a post with name than 5 simbols'),
    body('content').trim().isLength({min: 5, max: 250}).withMessage('Please enter a description more than 5 simbols and less than 250 simbols'),
    body('creator').trim().not().isEmpty()
      .isLength({min: 2})
      .withMessage('User id is required'),
  ];
};
router.get('/posts', isAuth, getPosts);
router.get('/post/:id', isAuth, getPostDetails);
router.post('/post', isAuth, validateParams(), createPost);

router.put('/post/:id', isAuth, validateParams(), changePost);

router.delete('/post/:id', isAuth, deletePost);
module.exports = router;
